stattach() {
    OUR_PID=$1
    if [[ -z $OUR_PID ]]; then
      printf "stattach <process-id>\n"
      return 1
    fi
    # track writes done to fd 1 & 2 aka stdout & stderr
    strace -p $OUR_PID -e write=1,2
}
